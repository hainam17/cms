import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';

class InfoGameSetting extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final int? flex1;
  final int? flex2;

  const InfoGameSetting({
    Key? key,
    this.title,
    this.subTitle,
    this.flex1,
    this.flex2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: flex1 ?? 7,
          child: Text(
            title ?? '',
            style:
                TextStyles.size12W400.copyWith(color: ColorsApp.color_3D3D3C),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Expanded(
          flex: flex2 ?? 3,
          child: Text(
            subTitle ?? '',
            style:
                TextStyles.size12W600.copyWith(color: ColorsApp.color_231508),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }
}
