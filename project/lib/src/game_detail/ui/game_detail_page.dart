import 'package:common/common.dart';
import 'package:example/common/base_button.dart';
import 'package:example/src/detailed_settings_game/ui/detailed_settings_game_page.dart';
import 'package:example/src/game_detail/ui/game_setting.dart';
import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class GameDetailPage extends StatelessWidget {
  const GameDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsApp.color_ffffff,
      appBar: AppBar(
        backgroundColor: ColorsApp.color_ffffff,
        title: Text(
          'Game Candy',
          style: TextStyles.size18W700.copyWith(color: ColorsApp.color_000000),
        ),
        centerTitle: true,
        elevation: 0,
        iconTheme: const IconThemeData(color: ColorsApp.color_000000),
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 12, bottom: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const GameSetting(),
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      'Chỉ số sản phẩm',
                      style: TextStyles.size20W600
                          .copyWith(color: ColorsApp.color_000000),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      'Số lượng người chơi',
                      style: TextStyles.size14W500
                          .copyWith(color: ColorsApp.color_000000),
                    ),
                    SfCartesianChart(
                      primaryXAxis: CategoryAxis(),
                      series: <SplineSeries<SalesData, String>>[
                        SplineSeries<SalesData, String>(
                          // Bind data source
                          dataSource: <SalesData>[
                            SalesData('Jan', 10),
                            SalesData('Feb', 15),
                            SalesData('Mar', 35),
                            SalesData('Apr', 40),
                            SalesData('May', 70),
                          ],
                          xValueMapper: (SalesData sales, _) => sales.year,
                          yValueMapper: (SalesData sales, _) => sales.sales,
                        ),
                        SplineSeries<SalesData, String>(
                          // Bind data source
                          dataSource: <SalesData>[
                            SalesData('Jan', 20),
                            SalesData('Feb', 15),
                            SalesData('Mar', 30),
                            SalesData('Apr', 45),
                            SalesData('May', 60),
                          ],
                          xValueMapper: (SalesData sales, _) => sales.year,
                          yValueMapper: (SalesData sales, _) => sales.sales,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    SfCartesianChart(
                      primaryXAxis: LogarithmicAxis(
                        borderColor: Colors.red,
                      ),
                      series: <SplineAreaSeries<TestData, int>>[
                        SplineAreaSeries<TestData, int>(
                          // Bind data source
                          dataSource: <TestData>[
                            TestData(1, 10),
                            TestData(2, 15),
                            TestData(3, 35),
                            TestData(4, 40),
                            TestData(5, 70),
                            TestData(6, 90),
                            TestData(7, 60),
                            TestData(8, 40),
                            TestData(9, 50),
                          ],
                          xValueMapper: (TestData sales, _) => sales.x,
                          yValueMapper: (TestData sales, _) => sales.y,
                          gradient: const LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: <Color>[
                              Color(0xff1f005c),
                              Color(0xff5b0060),
                              Color(0xff870160),
                              Color(0xffac255e),
                              Color(0xffca485c),
                              Color(0xffe16b5c),
                              Color(0xfff39060),
                              Color(0xffffb56b),
                            ],
                            // Gradient from https://learnui.design/tools/gradient-generator.html
                            tileMode: TileMode.mirror,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Doanh thu',
                      style: TextStyles.size14W500
                          .copyWith(color: ColorsApp.color_000000),
                    ),
                    Row(
                      children: [
                        Text(
                          '\$100',
                          style: TextStyles.size32W600
                              .copyWith(color: ColorsApp.color_000000),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: ColorsApp.color_4CBD6C..withOpacity(0.1),
                          ),
                          padding: const EdgeInsets.only(
                              left: 12, bottom: 8, right: 12, top: 8),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const Icon(
                                Icons.upgrade,
                                size: 12,
                                color: ColorsApp.color_00760E,
                              ),
                              Row(
                                children: [
                                  Text(
                                    '10%',
                                    style: TextStyles.size12W400.copyWith(
                                      color: ColorsApp.color_00510A,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    SfCircularChart(
                      series: <DoughnutSeries<RevenueData, String>>[
                        DoughnutSeries<RevenueData, String>(
                          dataSource: [
                            RevenueData('Jan', 10, ColorsApp.color_000000),
                            RevenueData('Feb', 20, ColorsApp.color_17A63F),
                            RevenueData('Mar', 30, ColorsApp.color_D11A1A),
                            RevenueData('Apr', 40, ColorsApp.color_484848),
                            RevenueData('May', 50, ColorsApp.color_F29339),
                            RevenueData('May', 60, ColorsApp.color_A352D9),
                          ],
                          dataLabelSettings:
                              const DataLabelSettings(isVisible: true),
                          xValueMapper: (RevenueData data, _) => data.year,
                          yValueMapper: (RevenueData data, _) => data.sales,
                          pointColorMapper: (RevenueData data, _) => data.color,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
            child: BaseButton(
              onTap: () {
                NavigatorManager.pushFullScreen(
                    context, const DetailedSettingsGamePage());
              },
              width: double.infinity,
              color: ColorsApp.color_E86305,
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              title: 'Tiếp theo',
              style: TextStyles.size16W600,
              radius: 4,
            ),
          )
        ],
      ),
    );
  }
}

class SalesData {
  SalesData(
    this.year,
    this.sales,
  );

  final String year;
  final double sales;
}

class RevenueData {
  RevenueData(
    this.year,
    this.sales,
    this.color,
  );

  final String year;
  final double sales;
  final Color? color;
}

class TestData {
  final int? x;
  final int? y;

  TestData(this.x, this.y);
}
