import 'package:common/common.dart';
import 'package:example/common/app_images.dart';
import 'package:example/common/base_button.dart';
import 'package:example/src/custom_game/ui/custom_game_page.dart';
import 'package:example/src/game_detail/component/info_game_setting.dart';
import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';

class GameSetting extends StatelessWidget {
  const GameSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Cài đặt game',
          style: TextStyles.size32W600.copyWith(color: ColorsApp.color_000000),
        ),
        const SizedBox(
          height: 4,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Candy Crush',
                    style: TextStyles.size20W600
                        .copyWith(color: ColorsApp.color_000000),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    'Thay đổi tài nguyên game bằng cách bấm nút \nTùy chỉnh',
                    style: TextStyles.size10W600
                        .copyWith(color: ColorsApp.color_958F8F),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BaseButton(
                        onTap: () {
                          NavigatorManager.pushFullScreen(
                              context, const CustomGamePage());
                        },
                        radius: 8,
                        color: ColorsApp.color_E86305,
                        title: 'Tùy Chỉnh',
                        style: TextStyles.size20W600
                            .copyWith(color: ColorsApp.color_ffffff),
                        padding: const EdgeInsets.only(
                            left: 24, right: 24, top: 8, bottom: 8),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                AppImages.imGameCandy,
                width: 120,
                height: 120,
                fit: BoxFit.fill,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 36,
        ),
        Text(
          'Cài đặt chỉ số',
          style: TextStyles.size20W600.copyWith(color: ColorsApp.color_000000),
        ),
        const SizedBox(
          height: 16,
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: ColorsApp.color_ffffff,
            border: Border.all(
              color: ColorsApp.color_FFDAB8,
              width: 1,
            ),
            boxShadow: [
              BoxShadow(
                color: ColorsApp.color_000000.withOpacity(0.1),
                spreadRadius: 0,
                blurRadius: 10,
                offset: const Offset(2, 3), // changes position of shadow
              ),
            ],
          ),
          padding:
              const EdgeInsets.only(top: 12, left: 12, right: 12, bottom: 20),
          child: Column(
            children: [
              InfoGameSetting(
                title: 'Số lượt chơi ban đầu',
                subTitle: 5.toString(),
              ),
              const SizedBox(
                height: 20,
              ),
              InfoGameSetting(
                title: 'Thời gian làm mới (phút)',
                subTitle: 1440.toString(),
              ),
              const SizedBox(
                height: 20,
              ),
              InfoGameSetting(
                title: 'Số lượt làm mới',
                subTitle: 5.toString(),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 32,
        ),
        Text(
          'Mô tả',
          style: TextStyles.size20W600.copyWith(color: ColorsApp.color_000000),
        ),
        const SizedBox(
          height: 4,
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: ColorsApp.color_ffffff,
            border: Border.all(
              color: ColorsApp.color_FFDAB8,
              width: 1,
            ),
            boxShadow: [
              BoxShadow(
                color: ColorsApp.color_000000.withOpacity(0.1),
                spreadRadius: 0,
                blurRadius: 10,
                offset: const Offset(2, 3), // changes position of shadow
              ),
            ],
          ),
          padding: const EdgeInsets.all(16),
          child: Text(
            'This charming pixel art puzzle game features adorable visuals across 72 puzzles, with each puzzle boasting its own unique logic that will put players wits to the test. Every puzzle features a new design where players have to get Chloe and Bunny safely to their Targets',
            style:
                TextStyles.size16W600.copyWith(color: ColorsApp.color_818181),
          ),
        ),
      ],
    );
  }
}
