import 'package:common/common.dart';
import 'package:example/common/app_images.dart';
import 'package:example/common/base_button.dart';
import 'package:example/common/common_text_form.dart';
import 'package:example/src/custom_game/ui/custom_game_page.dart';
import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';

class DetailedSettingsGamePage extends StatelessWidget {
  const DetailedSettingsGamePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsApp.color_ffffff,
          title: Text(
            'Quay lại',
            style:
                TextStyles.size15W600.copyWith(color: ColorsApp.color_E86305),
          ),
          iconTheme: const IconThemeData(color: ColorsApp.color_E86305),
          actions: [
            PopupMenuButton(
              itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                const PopupMenuItem(
                  child: Text('Item 1'),
                ),
                const PopupMenuItem(
                  child: Text('Item 2'),
                ),
                const PopupMenuItem(
                  child: Text('Item 3'),
                ),
              ],
              child: Image.asset(
                AppImages.icNotice,
                width: 20,
                height: 20,
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Image.asset(
              AppImages.imAvt,
              height: 36,
              width: 36,
            ),
            const SizedBox(
              width: 16,
            ),
          ],
          elevation: 0,
        ),
        body: Column(
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Divider(
                thickness: 1,
                color: ColorsApp.color_FFDAB8,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 28, right: 28, top: 8),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Chiến dịch A',
                        style: TextStyles.size12W600
                            .copyWith(color: ColorsApp.color_958F8F),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Cài đặt Game',
                        style: TextStyles.size24W500
                            .copyWith(color: ColorsApp.color_000000),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Candy Crush',
                                  style: TextStyles.size20W600
                                      .copyWith(color: ColorsApp.color_000000),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                const SizedBox(
                                  height: 4,
                                ),
                                Text(
                                  'Thay đổi tài nguyên game bằng cách bấm nút \nTùy chỉnh',
                                  style: TextStyles.size10W600
                                      .copyWith(color: ColorsApp.color_958F8F),
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                const SizedBox(
                                  height: 12,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    BaseButton(
                                      onTap: () {
                                        NavigatorManager.pushFullScreen(
                                            context, const CustomGamePage());
                                      },
                                      radius: 8,
                                      color: ColorsApp.color_E86305,
                                      title: 'Tùy Chỉnh',
                                      style: TextStyles.size20W600.copyWith(
                                          color: ColorsApp.color_ffffff),
                                      padding: const EdgeInsets.only(
                                          left: 24,
                                          right: 24,
                                          top: 8,
                                          bottom: 8),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              AppImages.imGameCandy,
                              width: 120,
                              height: 120,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 14,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: ColorsApp.color_ffffff,
                          border: Border.all(
                            color: ColorsApp.color_FFDAB8,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: ColorsApp.color_000000.withOpacity(0.1),
                              spreadRadius: 0,
                              blurRadius: 10,
                              offset: const Offset(
                                  2, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        padding:
                            const EdgeInsets.only(top: 12, right: 16, left: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Cài đặt Chỉ số',
                              style: TextStyles.size16W600
                                  .copyWith(color: ColorsApp.color_E86305),
                            ),
                            CommonForm(
                              control: TextEditingController(text: '5'),
                              heightSizeBox: 10,
                              style: TextStyles.size13W600
                                  .copyWith(color: ColorsApp.color_000000),
                              title: 'Số lượt chơi ban đầu',
                              titleStyle: TextStyles.size12W400
                                  .copyWith(color: ColorsApp.color_958F8F),
                              height: 4,
                              hint: 'Số lượt chơi ban đầu',
                              hintStyle: TextStyles.size13W600.copyWith(
                                color: ColorsApp.color_958F8F.withOpacity(0.5),
                              ),
                              fillcolor: ColorsApp.color_ffffff,
                              contentPadding: EdgeInsets.zero,
                              maxLength: 10,
                              textInputType: TextInputType.number,
                            ),
                            const Divider(
                              thickness: 1,
                              color: ColorsApp.color_C2C2C2,
                            ),
                            CommonForm(
                              control: TextEditingController(text: '1000'),
                              heightSizeBox: 10,
                              style: TextStyles.size13W600
                                  .copyWith(color: ColorsApp.color_000000),
                              title: 'Thời gian làm mới (phút)',
                              titleStyle: TextStyles.size12W400
                                  .copyWith(color: ColorsApp.color_958F8F),
                              height: 4,
                              hint: 'Thời gian làm mới',
                              hintStyle: TextStyles.size13W600.copyWith(
                                color: ColorsApp.color_958F8F.withOpacity(0.5),
                              ),
                              fillcolor: ColorsApp.color_ffffff,
                              contentPadding: EdgeInsets.zero,
                              maxLength: 10,
                            ),
                            const Divider(
                              thickness: 1,
                              color: ColorsApp.color_C2C2C2,
                            ),
                            CommonForm(
                              control: TextEditingController(text: '5'),
                              heightSizeBox: 10,
                              style: TextStyles.size13W600
                                  .copyWith(color: ColorsApp.color_000000),
                              title: 'Số lượt làm mới',
                              titleStyle: TextStyles.size12W400
                                  .copyWith(color: ColorsApp.color_958F8F),
                              height: 4,
                              hint: 'Số lượt làm mới',
                              hintStyle: TextStyles.size13W600.copyWith(
                                color: ColorsApp.color_958F8F.withOpacity(0.5),
                              ),
                              fillcolor: ColorsApp.color_ffffff,
                              contentPadding: EdgeInsets.zero,
                              maxLength: 10,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: ColorsApp.color_ffffff,
                          border: Border.all(
                            color: ColorsApp.color_FFDAB8,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: ColorsApp.color_000000.withOpacity(0.1),
                              spreadRadius: 0,
                              blurRadius: 10,
                              offset: const Offset(
                                  2, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mô tả',
                              style: TextStyles.size15W600
                                  .copyWith(color: ColorsApp.color_E86305),
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Text(
                              'This charming pixel art puzzle game features adorable visuals across 72 puzzles, with each puzzle boasting its own unique logic that will put players wits to the test. Every puzzle features a new design where players have to get Chloe and Bunny safely to their Targets.',
                              style: TextStyles.size12W500
                                  .copyWith(color: ColorsApp.color_958F8F),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 28, right: 28, bottom: 20),
              child: BaseButton(
                onTap: () {},
                width: double.infinity,
                color: ColorsApp.color_E86305,
                padding: const EdgeInsets.only(top: 12, bottom: 12),
                title: 'Tiếp theo',
                style: TextStyles.size16W600,
                radius: 4,
              ),
            ),
          ],
        ));
  }
}
