import 'package:common/common.dart';
import 'package:example/src/home/ui/home_page.dart';
import 'package:example/style/font_colors.dart';
import 'package:flutter/material.dart';

class AppHome extends StatefulWidget {
  const AppHome({Key? key}) : super(key: key);

  @override
  State<AppHome> createState() => _AppHomeState();
}

class _AppHomeState extends State<AppHome> {
  @override
  Widget build(BuildContext context) {
    return MainBottomNavigationBar(
      wantKeepAliveChildren: const [true, false, false],
      bottomNavigatorItemBuilder:
          (BuildContext contextNavigation, int currentIndex) {
        return BottomNavigationBar(
            currentIndex: currentIndex,
            onTap: (index) {
              contextNavigation.read<TabBarController>().tabIndex = index;
            },
            selectedItemColor: ColorsApp.color_17A63F,
            type: BottomNavigationBarType.fixed,
            selectedFontSize: 12,
            unselectedFontSize: 11,
            items: const [
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.import_contacts,
                    size: 25,
                    color: ColorsApp.color_C7C7C7,
                  ),
                  activeIcon: Icon(
                    Icons.import_contacts,
                    size: 25,
                    color: ColorsApp.color_17A63F,
                  ),
                  label: 'Chiến dịch'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.animation,
                    size: 25,
                    color: ColorsApp.color_C7C7C7,
                  ),
                  activeIcon: Icon(
                    Icons.animation,
                    size: 25,
                    color: ColorsApp.color_17A63F,
                  ),
                  label: 'Game'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.settings,
                    size: 25,
                    color: ColorsApp.color_C7C7C7,
                  ),
                  activeIcon: Icon(
                    Icons.settings,
                    size: 25,
                    color: ColorsApp.color_17A63F,
                  ),
                  label: 'Cài đặt'),
            ]);
      },
      children: [
        HomePage(),
        Scaffold(
          body: Center(
            child: Text('2'),
          ),
        ),
        Scaffold(
          body: Center(
            child: Text('3'),
          ),
        ),
      ],
    );
  }
}
