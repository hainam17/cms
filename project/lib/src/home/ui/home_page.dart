import 'package:common/common.dart';
import 'package:example/common/app_images.dart';
import 'package:example/common/base_button.dart';
import 'package:example/src/game_detail/ui/game_detail_page.dart';
import 'package:example/src/home/component/my_campaign.dart';
import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorsApp.color_ffffff,
        title: Text(
          'BRGames',
          style: TextStyles.size15W600.copyWith(color: ColorsApp.color_E77710),
        ),
        actions: [
          PopupMenuButton(
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              const PopupMenuItem(
                child: Text('Item 1'),
              ),
              const PopupMenuItem(
                child: Text('Item 2'),
              ),
              const PopupMenuItem(
                child: Text('Item 3'),
              ),
            ],
            child: Image.asset(
              AppImages.icNotice,
              width: 20,
              height: 20,
            ),
          ),
          const SizedBox(
            width: 12,
          ),
          Image.asset(
            AppImages.imAvt,
            height: 36,
            width: 36,
          ),
          const SizedBox(
            width: 16,
          ),
        ],
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Divider(
                thickness: 1,
                color: ColorsApp.color_FFDAB8,
              ),
              const SizedBox(
                height: 12,
              ),
              Text(
                'Chiến dịch của tôi (8)',
                style: TextStyles.size24W500
                    .copyWith(color: ColorsApp.color_000000),
              ),
              const SizedBox(
                height: 12,
              ),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      maxLines: 1,
                      textInputAction: TextInputAction.go,
                      decoration: InputDecoration(
                        enabledBorder: const UnderlineInputBorder(
                          borderSide: BorderSide(color: ColorsApp.color_FFDAB8),
                        ),
                        contentPadding: const EdgeInsets.only(top: 16),
                        hintText: 'Tìm kiếm...',
                        hintStyle: TextStyles.size11W400.copyWith(
                          color: ColorsApp.color_958F8F,
                          fontStyle: FontStyle.italic,
                        ),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(8),
                          child: Image.asset(
                            AppImages.icSearch,
                            width: 16,
                            height: 16,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 24,
                  ),
                  BaseButton(
                    onTap: () {},
                    color: ColorsApp.color_E86305,
                    padding: const EdgeInsets.only(
                        top: 8, bottom: 8, left: 12, right: 12),
                    style: TextStyles.size12W600,
                    title: 'Phân tích',
                    radius: 5,
                  )
                ],
              ),
              GridView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: const EdgeInsets.only(top: 12, bottom: 12),
                itemCount: 30,
                itemBuilder: (BuildContext context, int index) {
                  return SingleChildScrollView(
                    child: GestureDetector(
                      onTap: () {
                        NavigatorManager.pushFullScreen(
                            context, const GameDetailPage());
                      },
                      child: const MyCampaign(
                        image: AppImages.imGameCandy,
                        name: 'Tên chiến dịch',
                      ),
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 0,
                  mainAxisSpacing: 10,
                  mainAxisExtent: MediaQuery.of(context).size.width / 3,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
