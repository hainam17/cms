import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';

class MyCampaign extends StatelessWidget {
  final String? image;
  final String? name;

  const MyCampaign({
    Key? key,
    this.image,
    this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 100,
          width: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(16),
            child: Image.asset(
              image ?? '',
              fit: BoxFit.fill,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          name ?? '',
          style: TextStyles.size11W400.copyWith(color: ColorsApp.color_000000),
        )
      ],
    );
  }
}
