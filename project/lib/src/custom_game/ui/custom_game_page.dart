import 'package:common/common.dart';
import 'package:example/common/app_images.dart';
import 'package:example/common/base_button.dart';
import 'package:example/src/custom_game/bloc/product_bloc.dart';
import 'package:example/src/custom_game/component/campaign_setting.dart';
import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';

class CustomGamePage extends StatelessWidget {
  const CustomGamePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProductBloc(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsApp.color_ffffff,
          title: Text(
            'Quay lại',
            style:
                TextStyles.size15W600.copyWith(color: ColorsApp.color_E86305),
          ),
          iconTheme: const IconThemeData(color: ColorsApp.color_E86305),
          elevation: 0,
        ),
        body: Column(
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: Divider(
                thickness: 1,
                color: ColorsApp.color_E86305,
              ),
            ),
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 8, bottom: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Chiến dịch A',
                      style: TextStyles.size12W600
                          .copyWith(color: ColorsApp.color_958F8F),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            'Cài đặt chiến dịch',
                            style: TextStyles.size24W500
                                .copyWith(color: ColorsApp.color_000000),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        BaseButton(
                          onTap: () {},
                          title: 'Xem trước',
                          color: ColorsApp.color_E86305,
                          padding: const EdgeInsets.only(
                              top: 4, bottom: 4, right: 8, left: 8),
                          radius: 5,
                          style: TextStyles.size12W600,
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 36,
                    ),
                    Text(
                      'Hồ sơ chiến dịch và SEO',
                      style: TextStyles.size16W600
                          .copyWith(color: ColorsApp.color_3D3D3C),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    RichText(
                      text: TextSpan(
                        text: 'Thay đổi tài nguyên game bằng cách bấm nút',
                        style: TextStyles.size10W600
                            .copyWith(color: ColorsApp.color_958F8F),
                        children: [
                          TextSpan(
                            text: 'Tùy chỉnh.',
                            style: TextStyles.size10W600
                                .copyWith(color: ColorsApp.color_3D3D3C),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: ColorsApp.color_ffffff,
                        border: Border.all(color: ColorsApp.color_FFDAB8),
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            color: ColorsApp.color_000000.withOpacity(0.1),
                            spreadRadius: 0,
                            blurRadius: 10,
                            offset: const Offset(
                                2, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      padding: const EdgeInsets.all(14),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CampaignSetting(
                            title: 'Tiêu đề',
                            subTitle: 'Puzzle',
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const CampaignSetting(
                            title: 'Mô tả',
                            subTitle: 'Đây là mô tả của chiến dịch',
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          CampaignSetting(
                            child1: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Ảnh Chiến dịch',
                                  style: TextStyles.size12W400
                                      .copyWith(color: ColorsApp.color_3D3D3C),
                                ),
                                const SizedBox(
                                  height: 4,
                                ),
                                Text(
                                  'Hình ảnh này sẽ xuất hiện khi chia sẻ trên mạng xã hội.\nGợi ý: 1200 x 630 px',
                                  style: TextStyles.size10W400
                                      .copyWith(color: ColorsApp.color_958F8F),
                                )
                              ],
                            ),
                            child2: SizedBox(
                              width: 184,
                              height: 93,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child: Image.asset(
                                  AppImages.imCampaignSetting,
                                  // fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Text(
                      'URL của Chiến dịch',
                      style: TextStyles.size16W600
                          .copyWith(color: ColorsApp.color_3D3D3C),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    RichText(
                      text: TextSpan(
                        text: 'Thay đổi tài nguyên game bằng cách bấm nút ',
                        style: TextStyles.size10W600
                            .copyWith(color: ColorsApp.color_958F8F),
                        children: [
                          TextSpan(
                            text: 'Tùy chỉnh.',
                            style: TextStyles.size10W600
                                .copyWith(color: ColorsApp.color_3D3D3C),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border:
                            Border.all(color: ColorsApp.color_FFDAB8, width: 1),
                        borderRadius: BorderRadius.circular(5),
                        color: ColorsApp.color_ffffff,
                        boxShadow: [
                          BoxShadow(
                            color: ColorsApp.color_000000.withOpacity(0.1),
                            spreadRadius: 0,
                            blurRadius: 10,
                            offset: const Offset(
                                2, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CampaignSetting(
                            title: 'URL hiện tại',
                            flex1: 5,
                            subTitle: 'http://brandedgames.vn/puzzle22',
                            flex2: 5,
                          ),
                          const SizedBox(
                            height: 18,
                          ),
                          CampaignSetting(
                            title: 'URL tùy chỉnh',
                            flex1: 5,
                            child2: Column(
                              children: [
                                SizedBox(
                                  height: 36,
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 8, top: 4, right: 8, bottom: 4),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      hintText: 'Tên miền của bạn',
                                      hintStyle: TextStyles.size10W400.copyWith(
                                          color: ColorsApp.color_958F8F,
                                          fontStyle: FontStyle.italic),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                          color: ColorsApp.color_FFDAB8,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                          color: ColorsApp.color_FFDAB8,
                                        ),
                                      ),
                                      counterText: '',
                                    ),
                                    maxLength: 255,
                                  ),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                SizedBox(
                                  height: 36,
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 8, top: 4, right: 8, bottom: 4),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      hintText: 'Tên chiến dịch',
                                      hintStyle: TextStyles.size10W400.copyWith(
                                          color: ColorsApp.color_958F8F,
                                          fontStyle: FontStyle.italic),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                          color: ColorsApp.color_FFDAB8,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        borderSide: const BorderSide(
                                          color: ColorsApp.color_FFDAB8,
                                        ),
                                      ),
                                      counterText: '',
                                    ),
                                    maxLength: 255,
                                  ),
                                ),
                              ],
                            ),
                            flex2: 5,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
