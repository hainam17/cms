import 'package:example/style/font_colors.dart';
import 'package:flutter/material.dart';

class TextStyles {
  static const double size9 = 9;
  static const double size10 = 10;
  static const double size11 = 11;
  static const double size12 = 12;
  static const double size13 = 13;
  static const double size14 = 14;
  static const double size15 = 15;
  static const double size16 = 16;
  static const double size17 = 17;
  static const double size18 = 18;
  static const double size19 = 19;
  static const double size20 = 20;
  static const double size21 = 21;
  static const double size22 = 22;
  static const double size23 = 23;
  static const double size24 = 24;
  static const double size25 = 25;
  static const double size26 = 26;
  static const double size27 = 27;
  static const double size28 = 28;
  static const double size29 = 29;
  static const double size30 = 30;
  static const double size31 = 31;
  static const double size32 = 32;
  static const double size36 = 36;

  static const FontWeight w300 = FontWeight.w300;
  static const FontWeight w400 = FontWeight.w400;
  static const FontWeight w500 = FontWeight.w500;
  static const FontWeight w600 = FontWeight.w600;
  static const FontWeight w700 = FontWeight.w700;
  static const FontWeight w800 = FontWeight.w800;
  static const FontWeight w900 = FontWeight.w900;

  static TextStyle size12W400 = const TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size12,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size20W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size20,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size10W400UnderLine = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size10,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
    decoration: TextDecoration.underline,
  );
  static const TextStyle size14W500 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size14,
    fontWeight: w500,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size9W400 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size9,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size9W500 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size9,
    fontWeight: w500,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size10W400 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size10,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size13W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size13,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size10W500 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size10,
    fontWeight: w500,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size11W400 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size11,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size13W400 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size13,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size16W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size16,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size16W700 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size16,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size14W400 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size16,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size25W900 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size25,
    fontWeight: w900,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size12W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size12,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size15W700 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size12,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size11W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size11,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size32W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size32,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size9W300 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size9,
    fontWeight: w300,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size36W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size36,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size13W400UnderLine = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size13,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
    decoration: TextDecoration.underline,
  );
  static const TextStyle size13W600UnderLine = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size13,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
    decoration: TextDecoration.underline,
  );
  static const TextStyle size9W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size9,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size15W400 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size15,
    fontWeight: w400,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size20W900 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size20,
    fontWeight: w900,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size36W900 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size36,
    fontWeight: w900,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size18W700 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size18,
    fontWeight: w700,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size15W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size15,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size24W500 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size24,
    fontWeight: w500,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size10W600 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size10,
    fontWeight: w600,
    color: ColorsApp.color_ffffff,
  );
  static const TextStyle size12W500 = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: size12,
    fontWeight: w500,
    color: ColorsApp.color_ffffff,
  );
}
