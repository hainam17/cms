import 'package:flutter/material.dart';

class BaseButton extends StatelessWidget {
  final double? radius;
  final Color? color;
  final EdgeInsetsGeometry? padding;
  final String? title;
  final TextStyle? style;
  final GestureTapCallback? onTap;
  final double? width;

  const BaseButton({
    Key? key,
    this.radius,
    this.color,
    this.padding,
    this.title,
    this.style,
    this.onTap,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius ?? 0),
          color: color ?? Colors.white,
        ),
        padding: padding ?? EdgeInsets.zero,
        child: Center(
          child: Text(
            title ?? '',
            style: style,
          ),
        ),
      ),
    );
  }
}
