class AppImages {
  static const String imAvt = 'assets/images/im_avt.png';
  static const String imGameCandy = 'assets/images/im_game_candy.png';
  static const String imBanner = 'assets/images/im_banner.png';
  static const String icNotice = 'assets/icon/ic_notice.png';
  static const String icSearch = 'assets/icon/ic_search.png';
  static const String imCampaignSetting =
      'assets/images/im_campaign_setting.png';
}
