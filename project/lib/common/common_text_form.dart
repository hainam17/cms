import 'package:example/style/font_colors.dart';
import 'package:example/style/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CommonForm extends StatelessWidget {
  final String? title;
  final String? hint;
  final TextEditingController? control;
  final int? maxLine;
  final double? height;
  final int? maxLength;
  final TextInputType? textInputType;
  final List<TextInputFormatter>? inputFormatters;
  final Color? fillcolor;
  final TextStyle? titleStyle;
  final TextStyle? hintStyle;
  final double? heightSizeBox;
  final String? suffixText;
  final Widget? suffixIcon;
  final ValueChanged<String>? onChanged;
  final bool obscureText;
  final EdgeInsetsGeometry? contentPadding;
  final TextStyle? style;

  const CommonForm({
    Key? key,
    this.title,
    this.maxLine,
    this.hint,
    this.control,
    this.height,
    this.maxLength,
    this.textInputType,
    this.inputFormatters,
    this.fillcolor,
    this.titleStyle,
    this.hintStyle,
    this.heightSizeBox,
    this.suffixText,
    this.suffixIcon,
    this.onChanged,
    this.obscureText = false,
    this.contentPadding,
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: heightSizeBox ?? 16),
        if (title != null)
          Column(
            children: [
              Text(title ?? '',
                  style: titleStyle ??
                      TextStyles.size13W600
                          .copyWith(color: ColorsApp.color_000000)),
              SizedBox(
                height: height ?? 4,
              ),
            ],
          ),
        TextFormField(
          controller: control,
          obscureText: obscureText,
          style: style,
          decoration: InputDecoration(
            contentPadding: contentPadding ??
                const EdgeInsets.only(left: 16, right: 16, top: 13, bottom: 13),
            fillColor: fillcolor ?? ColorsApp.color_000000,
            filled: true,
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide.none,
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide.none,
              // borderSide: BorderSide(color: AppColors.color_6B6B6B),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            hintText: hint ?? '',
            hintStyle: hintStyle ??
                TextStyles.size12W400.copyWith(color: ColorsApp.color_000000),
            counterText: '',
            suffixIcon: suffixIcon ??
                Padding(
                  padding: const EdgeInsets.only(top: 13, bottom: 13),
                  child: Text(
                    suffixText ?? '',
                    style: TextStyles.size12W400,
                    textAlign: TextAlign.center,
                  ),
                ),
          ),
          maxLines: maxLine ?? 1,
          keyboardType: textInputType ?? TextInputType.text,
          maxLength: maxLength ?? 50,
          inputFormatters: inputFormatters,
          onChanged: onChanged,
        ),
      ],
    );
  }
}
